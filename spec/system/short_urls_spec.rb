# frozen_string_literal: true

require 'rails_helper'
require 'webdrivers'

# WebDrivers Gem
# https://github.com/titusfortner/webdrivers
#
# Official Guides about System Testing
# https://api.rubyonrails.org/v5.2/classes/ActionDispatch/SystemTestCase.html

RSpec.describe 'Short Urls', type: :system do
  before do
    driven_by :selenium, using: :chrome
    # If using Firefox
    # driven_by :selenium, using: :firefox
    #
    # If running on a virtual machine or similar that does not have a UI, use
    # a headless driver
    # driven_by :selenium, using: :headless_chrome
    # driven_by :selenium, using: :headless_firefox
  end

  describe 'index' do
    it 'shows a list of short urls' do
      urls = (1..10).to_a.map { |n| "https://google.com.br/q=#{n}" }
      urls.each { |url| FactoryBot.create(:url, original_url: url, short_url: SecureRandom.alphanumeric) }

      visit root_path

      expect(page).to have_text('HeyURL!')
      urls.each do |url|
        expect(page).to have_text(url)
      end
    end

    it 'shows a list with the latest 10 urls' do
      urls = (1..11).to_a.map { |n| "https://google.com.br/q=#{n}i" }
      urls.each { |url| FactoryBot.create(:url, original_url: url, short_url: SecureRandom.alphanumeric) }

      visit root_path

      expect(page).not_to have_text(urls.first)
    end
  end

  describe 'show' do
    it 'shows a panel of stats for a given short url' do
      FactoryBot.create(:url, original_url: 'https://google.com', short_url: 'ABCDF')
      visit url_path('ABCDF')

      expect(page).to have_text('/ABCDF')
    end

    context 'when not found' do
      it 'shows a 404 page' do
        visit url_path('NOTFOUND')

        expect(page).to have_text('Ops! url not found')
      end
    end
  end

  describe 'create' do
    context 'when url is valid' do
      it 'creates the short url' do
        visit '/'
        find('#url_original_url').fill_in(with: 'https://google.com')
        click_on 'Shorten URL'
        url = Url.last

        expect(url).to have_attributes(original_url: 'https://google.com')
      end

      it 'redirects to the home page' do
        visit '/'
        find('#url_original_url').fill_in(with: 'https://google.com')
        click_on 'Shorten URL'

        expect(page.current_path).to eq('/')
      end
    end

    context 'when url is invalid' do
      it 'does not create the short url and shows a message' do
        visit '/'
        find('#url_original_url').fill_in(with: 'invalid')
        click_on 'Shorten URL'

        expect(page).to have_text('Original url is invalid')
      end

      it 'redirects to the home page' do
        visit '/'
        find('#url_original_url').fill_in(with: 'invalid')
        click_on 'Shorten URL'

        expect(page.current_path).to eq('/')
      end
    end
  end

  describe 'visit' do
    it 'redirects the user to the original url' do
      url = FactoryBot.create(:url, original_url: 'https://www.google.com/', short_url: 'ABCDE')
      visit visit_path('ABCDE')

      expect(page.driver.current_url).to eq(url.original_url)
    end

    context 'when not found' do
      it 'shows a 404 page' do
        visit visit_path('NOTFOUND')

        expect(page).to have_text('Ops! url not found')
      end
    end

    it 'creates click when visiting url' do
      FactoryBot.create(:url, original_url: 'https://www.google.com/', short_url: 'ABCDE')

      expect { visit visit_path('ABCDE') }.to change(Click, :count).by(1)
    end

    it 'updates clicks_count when visiting url' do
      url =  FactoryBot.create(:url, original_url: 'https://www.google.com/', short_url: 'ABCDE', clicks_count: 0)

      visit visit_path('ABCDE')
      url.reload

      expect(url.clicks_count).to eq(1)
    end
  end
end
