# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TokenGenerator do
  describe '.generate_token' do
    it 'generates a 5 length token' do
      result = described_class.generate_token

      expect(result.length).to eq(5)
    end

    it 'and with only upcased letters' do
      result = described_class.generate_token

      expect(result).to match(/[A-Z]{5}/)
    end
  end

  describe '.generate_unique_token' do
    it 'generates a unique token' do
      FactoryBot.create(:url, short_url: 'EXIST')
      expect(described_class).to receive(:generate_token).and_return('EXIST')
      expect(described_class).to receive(:generate_token).and_call_original

      result = described_class.generate_unique_token

      expect(result).not_to eq('EXIST')
    end
  end
end
