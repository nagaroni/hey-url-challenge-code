# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Url, type: :model do
  describe 'validations' do
    it 'validates original URL is a valid URL' do
      model = Url.new(original_url: 'https://google.com.br', short_url: 'ABCDE')
      model.valid?

      expect(model.errors[:original_url]).not_to eq(['is invalid'])
    end

    it 'sets error message when URL is invalid' do
      model = Url.new(original_url: 'invalid', short_url: 'ABCDE')
      model.valid?

      expect(model.errors[:original_url]).to eq(['is invalid'])
    end

    it 'validates short URL is present' do
      model = Url.new(short_url: 'ABCDE')
      model.valid?

      expect(model.errors[:short_url]).not_to eq(["can't be blank"])
    end

    it 'validates short URL is not present' do
      model = Url.new(short_url: nil)
      model.valid?

      expect(model.errors[:short_url]).to eq(["can't be blank"])
    end
  end
end
