# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Click, type: :model do
  describe 'validations' do
    it 'validates url_id is valid' do
      model = described_class.new(url_id: 1)
      model.valid?

      expect(model.errors[:url_id]).not_to eq(["can't be blank"])
    end

    it 'validates browser is not null' do
      model = described_class.new(platform: 'linux')
      model.valid?

      expect(model.errors[:platform]).not_to eq(["can't be blank"])
    end

    it 'validates platform is not null' do
      model = described_class.new(browser: 'firefox')
      model.valid?

      expect(model.errors[:browser]).not_to eq(["can't be blank"])
    end
  end
end
