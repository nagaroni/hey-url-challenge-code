# frozen_string_literal: true

class Url < ApplicationRecord
  LATEST_LIMIT = 10

  scope :latest, -> { order(created_at: :desc).limit(LATEST_LIMIT) }

  validates :original_url, format: { with: %r{http(s)?://(www\.)?\w+\.[a-z]{2,3}(\.[a-z]{2,4})?}i }
  validates :short_url, presence: true

  has_many :clicks
end
