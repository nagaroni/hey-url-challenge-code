# frozen_string_literal: true

class Click < ApplicationRecord
  belongs_to :url

  validates :url_id, presence: true
  validates :browser, presence: true
  validates :platform, presence: true
end
