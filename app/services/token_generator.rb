# frozen_string_literal: true

class TokenGenerator
  def self.generate_token
    letters = []
    5.times do
      letters << ('A'..'Z').to_a.sample
    end
    letters.join
  end

  def self.generate_unique_token
    token = generate_token
    token = generate_token while Url.exists?(short_url: token)
    token
  end
end
