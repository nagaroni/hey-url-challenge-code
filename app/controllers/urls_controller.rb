# frozen_string_literal: true

class UrlsController < ApplicationController
  def index
    # recent 10 short urls
    @url = Url.new
    @urls = Url.latest
  end

  def create
    url = Url.new(params.require(:url).permit(:original_url))
    url.short_url = TokenGenerator.generate_unique_token
    flash[:notice] = url.errors.full_messages.join(' ') unless url.save

    redirect_to root_path
  end

  def show
    @url = Url.find_by(short_url: params[:url])

    return render plain: 'Ops! url not found', status: :not_found if @url.blank?

    # implement queries
    @daily_clicks = @url.clicks.group('DATE(created_at)').count.transform_keys(&:to_s).to_a
    @browsers_clicks = @url.clicks.group(:browser).count.to_a
    @platform_clicks = @url.clicks.group(:platform).count.to_a
  end

  def visit
    url = Url.find_by(short_url: params[:short_url])
    if url
      url.clicks.create(platform: browser.platform.name, browser: browser.name)
      url.update(clicks_count: url.clicks.count)

      redirect_to url.original_url
    else
      render plain: 'Ops! url not found', status: :not_found
    end
  end
end
